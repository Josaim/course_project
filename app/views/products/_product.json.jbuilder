json.extract! product, :id, :title, :description, :price, :country, :kind_tea, :sort_tea, :created_at, :updated_at
json.url product_url(product, format: :json)
