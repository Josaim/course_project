class Product < ApplicationRecord
  before_destroy :not_referenced_by_any_line_item
  mount_uploader :image, ImageUploader
  serialize :image, JSON
  belongs_to :customer, optional: true
  has_many :line_items

  COUNTRY = %w{Китай Россия Индия Шриланка Япония}
  KIND_TEA = %w{ Рассыпной Прессованный}
  SORT_TEA = %w{Красный Зеленый Белый Улун Пуэр Желтый}

  private

  def not_referenced_by_any_line_item
    unless line_items.empty?
      errors.add(:base, "Текущий итем")
      throw :abort

    end
  end
end

