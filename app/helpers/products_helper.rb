module ProductsHelper

def product_author(product)
    customer_signed_in? && current_customer.id == product.customer_id
end

end
