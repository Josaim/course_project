Rails.application.routes.draw do
  resources :line_items
  resources :carts
  resources :products
  get 'catalog' => 'pages_controller#catalog'
  get 'delivery' => 'pages_controller#delivery'
  get 'contact' => 'pages_controller#contact'
  root 'products#index'
  devise_for :customers, controllers: {
    regisrations: 'regisrations'
  }

  devise_scope :customer do
    get "/login" => "devise/sessions#new"
    delete "/logout" => "devise/sessions#destroy"
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
