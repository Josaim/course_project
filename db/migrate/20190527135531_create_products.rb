class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :title
      t.text :description
      t.integer :price, precision: 5,scale: 2, default:0
      t.string :country
      t.string :kind_tea
      t.string :sort_tea

      t.timestamps
    end
  end
end
