class AddSmallDescriptionToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :small_description, :string
  end
end
