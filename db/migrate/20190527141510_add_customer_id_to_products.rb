class AddCustomerIdToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :customer_id, :integer
  end
end
